<?php

namespace App\Repositories;

use App\Repositories\courseInterface;
use App\Http\Requests\CreateCourseRequest;
use App\Models\Course;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CourseRepository implements courseInterface
{
    public function getCourse($id){

        try{

            $course = Course::findOrFail($id);

        }catch(ModelNotFoundException $e){

            return [
                'success' => false,
                'message' => 'Entry not found.',
                'error' => $e->getMessage()
            ];
        }

        return $course;
    }

    public function createCourse( CreateCourseRequest $request ){

        try{

            $course = Course::create($request->validated());

        }catch(\Illuminate\Database\QueryException $e){

            return [
                'success' => false,
                'error' => $e->errorInfo
            ];
        }

        return [
            'success' => true,
            'course' => $course
        ];
    
    }

    public function editCourse($id, $request){

        /** search for entry */
        try{

            $course = Course::findOrFail($id);

        }catch(ModelNotFoundException $e){

            return [
                'success' => false,
                'message' => 'Entry not found.',
                'error' => $e->getMessage()
            ];
        }

        $coursePrimaryKey = (new Course)->getKeyname();

        /** if entry exists update it */
        try{

            $request = app(CreateCourseRequest::class);
            $updatedCourse = Course::where($coursePrimaryKey, $id)->update($request->validated());

        }catch(\Illuminate\Database\QueryException $e){

            return [
                'success' => false,
                'error' => $e->errorInfo
            ];
        }

        return [
            'success' => true,
            'course' => Course::find($id)
        ];

        
    }

    public function deleteCourse($id){

        try{

            $course = Course::findOrFail($id);

        }catch(ModelNotFoundException $e){

            return [
                'success' => false,
                'message' => 'Entry not found.',
                'error' => $e->getMessage()
            ];
        }

        $deleted =  $course->delete();

        if($deleted) {

            return [
                'success' => true,
                'messages' => 'Entry soft deleted',
                'course' => $course
            ];

        } 

        return false;
    }

    public function allCourses(){

        return Course::all();
    }
}