<?php

namespace App\Repositories;

use App\Http\Requests\CreateCourseRequest;

interface courseInterface
{
    public function getCourse($id);
    public function createCourse(CreateCourseRequest $data);
    public function editCourse($id, $data);
    public function deleteCourse($id);
    public function allCourses();
}