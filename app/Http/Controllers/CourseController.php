<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourseRequest;
use Illuminate\Http\Request;
use App\Repositories\CourseRepository;

class CourseController extends Controller
{
    private $courseRepository;

    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function getCourse(Request $request)
    {
        $response = null;
        $method = $request->method();

        switch($method)
        {
            case "GET":
                $response = $this->courseRepository->allCourses();
                break;
            case "POST":
               $request = app(CreateCourseRequest::class);
               $response = $this->courseRepository->createCourse( $request );
                break;
            default:
                break;
        }
        
        return response()->json($response);
    }

    public function editCourse(Request $request)
    {
        $response = null;
        $method = $request->method();

        switch($method)
        {
            case "GET":
                $response = $this->courseRepository->getCourse($request->id);
                break;
            case "PUT":
                $courseId = $request->id;
                $response = $this->courseRepository->editCourse($courseId, $request);
                break;
            case "DELETE":
                $response = $this->courseRepository->deleteCourse($request->id);
                break;
        }

        return response()->json($response);
    }
}
