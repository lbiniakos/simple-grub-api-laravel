<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class CreateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|min:4',
            'description' => 'sometimes|required|min:4',
            'status' => 'sometimes|required|digits_between:0,1',
            'is_premium' => 'sometimes|required|digits_between:0,1'
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => __('Title is required'),
            'title.min' => __('Title needs at least 4 characters'),
            'description.min' => __('Description needs at least 4 characters'),
            'status.digits_between' => __('Status needs to be 0 or 1'),
            'is_premium.digits_between' => __('Is premium need to be 0 or 1')
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'errors' => $validator->errors()
                ], 
            422)
        );
    }

}
